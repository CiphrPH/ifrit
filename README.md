# Ifrit

This is the front end for Ciphr

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

```
$ git clone git@gitlab.com:ciphr/ifrit.git
$ cd ifrit
$ npm install
$ bower install
```

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

## Code Generators

Make use of the many generators for code, try `ember help generate` for more details

## Running Tests

* `ember test`
* `ember test --server`

## Building

* `ember build` (development)
* `ember build --environment production` (production)

## Deploying

### Initial Deployment

1. On initial deployment, make sure to finish provisioning the server via [Hydaelyn](https://gitlab.com/ciphr/hydaelyn)
before doing anything else.

2. Create `.env.deploy.staging` or `.env.deploy.production` and provide the following values:

```
PRIVATE_KEY_FILE=/absolute/path/to/ssh/key
```

### Subsequent Deployment

Subsequent deployments can be done using the following command:

```
$ ember deploy <staging|production>
```

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

